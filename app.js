module.exports = function() {
  const http = require('http');
  const https = require('https');
  const fs = require('fs');
  const serverSessionId = randomString();

  function randomString() {
    return Math.random().toString(36).substring(2, 15);
  }

  function createOrder() {
    return httpPostRequest(
      'initialise',
      {
        // customer reference will be unique to each server start
        customerRef: 'javascript-demo_' + serverSessionId,
        amount: 4000,
      },
    );
  }
  function requestClientToken() {
    return httpPostRequest(
      'client-token',
      {
        // customer reference will be unique to each server start
        customerRef: 'javascript-demo_' + serverSessionId,
      },
    );
  }

  function httpPostRequest(endpoint, requestBody) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    return new Promise(resolve => {
      // This demo app uses a merchant simulator backend, hosted at the following domain.
      // This is for demonstration purposes only. Outside of this demonstration, it will be replaced by a call to your own API
      const request = https.request(`https://web.qa.au.littlepay.com/api/v1/merchantwebapi/${endpoint}`,
        options, res => {
          let data = '';
          res.on('data', chunk => data += chunk);
          res.on('end', () => resolve(JSON.parse(data)));
        });

      request.write(JSON.stringify(requestBody));
      request.end();
    });
  }

  http.createServer(function({ url }, res) {
    if (url === '/client-token') {
      requestClientToken()
        .then(({ clientToken }) => {
          res.writeHead(200, { 'Content-Type': 'application/json' });
          res.end(JSON.stringify({ clientToken }));
        });
    } else if (url === '/checkout-data') {
      createOrder()
        .then(({ clientToken }) => {
          res.writeHead(200, { 'Content-Type': 'application/json' });
          res.end(JSON.stringify({ clientToken }));
        });
    } else if (url.startsWith('/img/')) {
      fs.readFile(`./${url}`, (err, data) => {
        res.writeHead(200, {'Content-Type': 'image/png'});
        res.end(data)
      })
    } else {
      require('fs');
      let htmlPage = url.endsWith('.html') ? url.match(/\/(\w+\.html)/)[1] : 'home.html'

      fs.readFile(`./${htmlPage}`, function(err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html', 'Content-Length': data.length });
        res.end(data);
      });
    }
  }).listen(3000, function() {
    console.log('littlepay sample app running at localhost:3000');
  });
};
