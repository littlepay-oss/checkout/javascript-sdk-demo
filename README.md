# Littlepay Javascript Demo

| :exclamation: Important          |
|:---------------------------|
| **Do not** input real credit/debit card data or real PayPal accounts when using this demo app.

____
This is a simple merchant webpage app that demonstrates an implementation of [littlepay-javascript sdk]('https://checkout.docs.littlepay.io/docs/checkout-javascript-sdk').

To run the app you will need:

1. Node.js runtime (https://nodejs.org/en/download/)
2. A modern web browser to test the webpage
3. _(optional)_ Test credit/debit cards to test card payment - card numbers are available in [Littlepay shared documentation]('https://checkout.docs.littlepay.io/docs/shared-documentation')
4. _(optional)_ PayPal Sandbox account to test PayPal payments. See [guidance on creating an account]('https://developer.paypal.com/docs/api/sandbox/accounts/')
 
### 1) Clone this repository
```shell script
git clone https://gitlab.com/littlepay-oss/checkout/javascript-sdk-demo.git
cd javascript-sdk-demo
```

### 2) Run the demo server 
```shell script
node ./demo.js
```
(Alternatively, you can use `npm run demo` instead of `node ./demo.js`)

### 3) Test the running application in a browser
In a web browser, visit `localhost:3000`. You should see demo merchant homepage. 
Looking something like this:

<img src="screenshot-merchant-homepage.png" alt="Demo merchant homepage screenshot" width="500"> 

### 4) Test Checkout
The address form is for display purposes only and populating the form does not affect SDK behaviour. 

Click or tap '_Continue to checkout_' button, underneath the address form. This should display the littlepay-javascript SDK ui. Something like this:

<img src="screenshot-payment-methods.png" alt="Demo payment methods screenshot" width="500">

## Related Documentation
Browse the other Littlepay repositories for documentation relating to SDKs and APIs - https://checkout.docs.littlepay.io/docs/checkout-javascript-sdk
